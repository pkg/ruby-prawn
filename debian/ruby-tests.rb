require "rspec/autorun"
$LOAD_PATH.delete(File.expand_path("../../lib",__FILE__))
$LOAD_PATH << File.expand_path("../../spec",__FILE__)

RSpec.configure do |c|
  c.filter_run_excluding :unresolved => true
end

require "spec/spec_helper"
Dir['./spec/**/*_spec.rb'].select {|f| f !~ /manual_spec/}.each { |f| require f }
