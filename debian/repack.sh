#!/bin/sh
# repack.sh - repacks Prawn's upstream tarball to remove embedded non-free
# fonts or fonts available as Debian packages, and images with unclear
# licensing.

set -e

DEB_UPSTREAM_VERSION=$(dpkg-parsechangelog | sed -rne 's/^Version: ([^+]+).*/\1/p')
IMGDIR=prawn-$DEB_UPSTREAM_VERSION/data/images
FONTDIR=prawn-$DEB_UPSTREAM_VERSION/data/fonts

uscan --noconf --verbose --force-download --rename --download-current-version  --destdir=.

rm -rf prawn-${DEB_UPSTREAM_VERSION}

# unpack upstream tarball
tar -xf ruby-prawn_${DEB_UPSTREAM_VERSION}.orig.tar.gz
#mv prawn-* prawn-$DEB_UPSTREAM_VERSION
rm ruby-prawn_${DEB_UPSTREAM_VERSION}.orig.tar.gz

# delete truetype fonts and Action Man.dfont
for x in $FONTDIR/*.ttf
do
       rm $x
done

# remove content of needed images with problematic license or without license
# requires imagemagick
convert $IMGDIR/pigs.jpg\
	-fill white -draw "color 0,0 reset" \
	-fill red -draw "rectangle 0,0 10,10" \
	-fill blue -draw "rectangle 0,10 10,20" \
	-fill green -draw "rectangle 10,0 20,10" \
	-fill black -pointsize 18\
	-draw "text 20,40 'content removed'"\
	-draw "text 20,60 'Please consult README.source'"\
	-draw "text 20,80 'in /usr/share/doc/ruby-prawn/'" $IMGDIR/pigs.jpg 

convert $IMGDIR/fractal.jpg\
	-fill white -draw "color 0,0 reset" \
	-fill red -draw "rectangle 0,0 10,10" \
	-fill blue -draw "rectangle 0,10 10,20" \
	-fill green -draw "rectangle 10,0 20,10" \
	-fill black -pointsize 14\
	-draw "text 20,40 'content removed'"\
	-draw "text 20,55 'Please consult README.source'"\
	-draw "text 20,70 'in /usr/share/doc/ruby-prawn/'" $IMGDIR/fractal.jpg 

convert $IMGDIR/stef.jpg\
	-fill white -draw "color 0,0 reset"\
	-fill black -pointsize 8\
	-draw "text 0,10 'content removed'"\
	-draw "text 0,20 'see README.source'"\
	-draw "text 0,30 'in /usr/share/\\'"\
	-draw "text 0,40 '  doc/ruby-prawn'" $IMGDIR/stef.jpg

# remove images not needed for the manual and data until their license is made clear.
rm -f $IMGDIR/16bit.* $IMGDIR/arrow* $IMGDIR/dice* $IMGDIR/letter*
rm -f $IMGDIR/page* $IMGDIR/ruport* $IMGDIR/tru256.bmp $IMGDIR/web-links*

# repack the tarball
mv prawn-$DEB_UPSTREAM_VERSION ruby-prawn-$DEB_UPSTREAM_VERSION.orig
GZIP=--best tar -cz --owner root --group root --mode a+rX -f \
	../ruby-prawn_$DEB_UPSTREAM_VERSION+dfsg.orig.tar.gz ruby-prawn-$DEB_UPSTREAM_VERSION.orig
rm -r ruby-prawn-$DEB_UPSTREAM_VERSION.orig

